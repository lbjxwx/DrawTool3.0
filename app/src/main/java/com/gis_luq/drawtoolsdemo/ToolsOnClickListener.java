package com.gis_luq.drawtoolsdemo;

import android.content.Context;
import android.view.View;

import com.esri.android.map.MapView;
import com.esri.core.map.Graphic;
import com.gis_luq.lib.Draw.DrawTool;

/**
 * 绘图点击事件
 * Created by gis-luq on 2016/1/2.
 */
public class ToolsOnClickListener implements View.OnClickListener {

    private Context context = null;
    private DrawTool drawTool = null;
    private Graphic selectGraphic =null;
    private MapView mapView = null;

    public ToolsOnClickListener(Context context, DrawTool drawTool, Graphic selectGraphic, MapView mapView) {
        this.context = context;
        this.drawTool = drawTool;
        this.selectGraphic = selectGraphic;
        this.mapView = mapView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDrawPoint://绘制点
                drawTool.activate(DrawTool.POINT);
                break;
            case R.id.btnDrawPolyline://绘制线
                drawTool.activate(DrawTool.POLYLINE);
                break;
            case R.id.btnDrawFreePolyline://绘制流状线
                drawTool.activate(DrawTool.FREEHAND_POLYLINE);
                break;
            case R.id.btnDrawPolygon://绘制面
                drawTool.activate(DrawTool.POLYGON);
                break;
            case R.id.btnDrawFreePolygon://绘制流状面
                drawTool.activate(DrawTool.FREEHAND_POLYGON);
                break;
            case R.id.btnDrawCircle://绘制圆
                drawTool.activate(DrawTool.CIRCLE);
                break;
            case R.id.btnDrawEnvlope://绘制矩形
                drawTool.activate(DrawTool.ENVELOPE);
                break;
            case R.id.btnDrawSave://保存
                drawTool.sendDrawEndEvent();
                break;
            case R.id.btnDrawUndo://回退
                if (drawTool.isActive()){
                    drawTool.actionUndo();
                }
                break;
            case R.id.btnDrawDeleteNode://删除节点
                if (drawTool.isActive()){
                    drawTool.actionDelete();
                }
                break;
        }

    }
}
